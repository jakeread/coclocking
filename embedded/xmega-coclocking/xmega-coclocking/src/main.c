/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>

// LEDS PD3 and PD3
// Button PD5

void usb_init(void){
	udc_start();
}

static bool cdc_ok = false;

bool callback_cdc_enable(void){
	cdc_ok = true;
	return true;
}

void callback_cdc_disable(void){
	cdc_ok = false;
}

void callback_rx_notify(uint8_t port){
	char character = udi_cdc_getc();
	//PORTA.OUTSET = character;
	//PORTA.OUTCLR = ~character;
	if(cdc_ok){
		udi_cdc_putc(character);
	}
}

#define IS_TX_BOARD 1

#if IS_TX_BOARD
#warning TXBOARD

int main (void)
{
	sysclk_init();
	irq_initialize_vectors();
	cpu_irq_enable();
	board_init();
	
	usb_init();
		
	PORTD.DIRSET = PIN3_bm | PIN4_bm; // set output (leds)
	PORTD.DIRCLR = PIN5_bm; // set input (button)
	
	PORTA.DIRSET = PIN0_bm | PIN1_bm | PIN2_bm | PIN3_bm | PIN4_bm | PIN5_bm | PIN6_bm | PIN7_bm;
	
	PORTC.DIRSET = PIN0_bm; // new data out
	PORTC.DIRCLR = PIN1_bm; // fpga ready for new data
	
	uint8_t counter = 0;
	while(1){
		if(counter > 255){
			counter = 0;
		}
		PORTA.OUT = counter;
		PORTC.OUTSET = PIN0_bm; // set hi to indicate new data
		delay_ms(1); // should be already doing the business
		PORTC.OUTCLR = PIN0_bm;
		delay_ms(100);
		counter ++;
	}
}

#elif IS_RX_BOARD

#warning RXBOARD

int main (void)
{
	sysclk_init();
	irq_initialize_vectors();
	cpu_irq_enable();
	board_init();
	
	usb_init();
	
	PORTD.DIRSET = PIN3_bm | PIN4_bm; // set output (leds)
	PORTD.DIRCLR = PIN5_bm; // set input (button)
	
	PORTA.DIRCLR = PIN0_bm | PIN1_bm | PIN2_bm | PIN3_bm | PIN4_bm | PIN5_bm | PIN6_bm | PIN7_bm;
	
	PORTC.DIRSET = PIN0_bm; // new data out
	PORTC.DIRCLR = PIN1_bm; // fpga ready for new data
	
	while(1){
	}
}

#else

#warning which board are you?

#endif