[ActiveSupport MAP]
Device = LCMXO2-1200HC;
Package = QFN32;
Performance = 4;
LUTS_avail = 1280;
LUTS_used = 11;
FF_avail = 1302;
FF_used = 11;
INPUT_LVCMOS25 = 2;
OUTPUT_LVCMOS25 = 8;
IO_avail = 22;
IO_used = 10;
EBR_avail = 7;
EBR_used = 0;
