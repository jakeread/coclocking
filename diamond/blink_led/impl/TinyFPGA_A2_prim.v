// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.10.0.111.2
// Netlist written on Mon Dec 04 00:42:57 2017
//
// Verilog Description of module TinyFPGA_A2
//

module TinyFPGA_A2 (DOUT, DIN, CLKIN, TRGIN) /* synthesis syn_module_defined=1 */ ;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(1[8:19])
    output [7:0]DOUT;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(2[15:19])
    input DIN;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(3[8:11])
    input CLKIN;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(4[8:13])
    input TRGIN;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(5[8:13])
    
    wire CLKIN_c /* synthesis SET_AS_NETWORK=CLKIN_c, is_clock=1 */ ;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(4[8:13])
    
    wire GND_net, VCC_net, DOUT_c_7, DOUT_c_6, DOUT_c_5, DOUT_c_4, 
        DOUT_c_3, DOUT_c_2, DOUT_c_1, DOUT_c_0, DIN_c, n18;
    wire [3:0]counter;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(10[12:19])
    
    wire CLKIN_c_enable_6, CLKIN_c_enable_5, CLKIN_c_enable_1, CLKIN_c_enable_3, 
        n1, n19, CLKIN_c_enable_4, CLKIN_c_enable_8, CLKIN_c_enable_7, 
        CLKIN_c_enable_2;
    
    VHI i2 (.Z(VCC_net));
    FD1P3AX dout_i3 (.D(DIN_c), .SP(CLKIN_c_enable_1), .CK(CLKIN_c), .Q(DOUT_c_2));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(22[9] 29[6])
    defparam dout_i3.GSR = "ENABLED";
    LUT4 i142_2_lut_3_lut (.A(counter[1]), .B(counter[2]), .C(counter[0]), 
         .Z(CLKIN_c_enable_7)) /* synthesis lut_function=(!(A+(B+(C)))) */ ;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(27[3:16])
    defparam i142_2_lut_3_lut.init = 16'h0101;
    LUT4 i139_2_lut_3_lut (.A(counter[1]), .B(counter[2]), .C(counter[0]), 
         .Z(CLKIN_c_enable_8)) /* synthesis lut_function=(!(A+(B+!(C)))) */ ;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(27[3:16])
    defparam i139_2_lut_3_lut.init = 16'h1010;
    FD1P3AX dout_i4 (.D(DIN_c), .SP(CLKIN_c_enable_2), .CK(CLKIN_c), .Q(DOUT_c_3));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(22[9] 29[6])
    defparam dout_i4.GSR = "ENABLED";
    FD1S3AX counter_31_32__i3 (.D(n18), .CK(CLKIN_c), .Q(counter[2]));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(28[14:25])
    defparam counter_31_32__i3.GSR = "ENABLED";
    FD1P3AX dout_i5 (.D(DIN_c), .SP(CLKIN_c_enable_3), .CK(CLKIN_c), .Q(DOUT_c_4));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(22[9] 29[6])
    defparam dout_i5.GSR = "ENABLED";
    OB DOUT_pad_0 (.I(DOUT_c_0), .O(DOUT[0]));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(2[15:19])
    FD1P3AX dout_i6 (.D(DIN_c), .SP(CLKIN_c_enable_4), .CK(CLKIN_c), .Q(DOUT_c_5));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(22[9] 29[6])
    defparam dout_i6.GSR = "ENABLED";
    OB DOUT_pad_1 (.I(DOUT_c_1), .O(DOUT[1]));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(2[15:19])
    OB DOUT_pad_2 (.I(DOUT_c_2), .O(DOUT[2]));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(2[15:19])
    OB DOUT_pad_3 (.I(DOUT_c_3), .O(DOUT[3]));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(2[15:19])
    FD1P3AX dout_i7 (.D(DIN_c), .SP(CLKIN_c_enable_5), .CK(CLKIN_c), .Q(DOUT_c_6));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(22[9] 29[6])
    defparam dout_i7.GSR = "ENABLED";
    FD1P3AX dout_i8 (.D(DIN_c), .SP(CLKIN_c_enable_6), .CK(CLKIN_c), .Q(DOUT_c_7));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(22[9] 29[6])
    defparam dout_i8.GSR = "ENABLED";
    LUT4 i127_2_lut_3_lut (.A(counter[1]), .B(counter[2]), .C(counter[0]), 
         .Z(CLKIN_c_enable_5)) /* synthesis lut_function=(!(((C)+!B)+!A)) */ ;
    defparam i127_2_lut_3_lut.init = 16'h0808;
    OB DOUT_pad_4 (.I(DOUT_c_4), .O(DOUT[4]));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(2[15:19])
    FD1S3AX counter_31_32__i1 (.D(n1), .CK(CLKIN_c), .Q(counter[0]));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(28[14:25])
    defparam counter_31_32__i1.GSR = "ENABLED";
    FD1S3AX counter_31_32__i2 (.D(n19), .CK(CLKIN_c), .Q(counter[1]));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(28[14:25])
    defparam counter_31_32__i2.GSR = "ENABLED";
    OB DOUT_pad_5 (.I(DOUT_c_5), .O(DOUT[5]));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(2[15:19])
    LUT4 i84_2_lut_3_lut (.A(counter[1]), .B(counter[2]), .C(counter[0]), 
         .Z(CLKIN_c_enable_6)) /* synthesis lut_function=(A (B (C))) */ ;
    defparam i84_2_lut_3_lut.init = 16'h8080;
    LUT4 i103_3_lut (.A(counter[2]), .B(counter[1]), .C(counter[0]), .Z(n18)) /* synthesis lut_function=(!(A (B (C))+!A !(B (C)))) */ ;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(28[14:25])
    defparam i103_3_lut.init = 16'h6a6a;
    OB DOUT_pad_6 (.I(DOUT_c_6), .O(DOUT[6]));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(2[15:19])
    LUT4 i45_1_lut (.A(counter[0]), .Z(n1)) /* synthesis lut_function=(!(A)) */ ;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(27[3:16])
    defparam i45_1_lut.init = 16'h5555;
    LUT4 i96_2_lut (.A(counter[1]), .B(counter[0]), .Z(n19)) /* synthesis lut_function=(!(A (B)+!A !(B))) */ ;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(28[14:25])
    defparam i96_2_lut.init = 16'h6666;
    LUT4 i132_2_lut_3_lut (.A(counter[1]), .B(counter[2]), .C(counter[0]), 
         .Z(CLKIN_c_enable_3)) /* synthesis lut_function=(!(A+((C)+!B))) */ ;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(27[3:16])
    defparam i132_2_lut_3_lut.init = 16'h0404;
    LUT4 i129_2_lut_3_lut (.A(counter[1]), .B(counter[2]), .C(counter[0]), 
         .Z(CLKIN_c_enable_4)) /* synthesis lut_function=(!(A+!(B (C)))) */ ;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(27[3:16])
    defparam i129_2_lut_3_lut.init = 16'h4040;
    FD1P3AX dout_i1 (.D(DIN_c), .SP(CLKIN_c_enable_7), .CK(CLKIN_c), .Q(DOUT_c_0));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(22[9] 29[6])
    defparam dout_i1.GSR = "ENABLED";
    LUT4 i134_2_lut_3_lut (.A(counter[2]), .B(counter[1]), .C(counter[0]), 
         .Z(CLKIN_c_enable_2)) /* synthesis lut_function=(!(A+!(B (C)))) */ ;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(27[3:16])
    defparam i134_2_lut_3_lut.init = 16'h4040;
    LUT4 i137_2_lut_3_lut (.A(counter[2]), .B(counter[1]), .C(counter[0]), 
         .Z(CLKIN_c_enable_1)) /* synthesis lut_function=(!(A+((C)+!B))) */ ;   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(27[3:16])
    defparam i137_2_lut_3_lut.init = 16'h0404;
    VLO i143 (.Z(GND_net));
    FD1P3AX dout_i2 (.D(DIN_c), .SP(CLKIN_c_enable_8), .CK(CLKIN_c), .Q(DOUT_c_1));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(22[9] 29[6])
    defparam dout_i2.GSR = "ENABLED";
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    TSALL TSALL_INST (.TSALL(GND_net));
    IB CLKIN_pad (.I(CLKIN), .O(CLKIN_c));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(4[8:13])
    IB DIN_pad (.I(DIN), .O(DIN_c));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(3[8:11])
    OB DOUT_pad_7 (.I(DOUT_c_7), .O(DOUT[7]));   // d:/dropbox (personal)/cba/projects/coclocking/diamond/blink_led/tinyfpga_a2.v(2[15:19])
    GSR GSR_INST (.GSR(VCC_net));
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

