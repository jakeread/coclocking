module TinyFPGA_A2 (
	output [7:0] DOUT, // parallel data in port 
	input DIN, // serial data out
	input CLKIN, // literal clock
	input TRGIN // starts frame
);
	
	reg dhold;
	reg [7:0] dout; // 8-bit wide input port
	reg [3:0] counter; // counts to 8
	
	always @(posedge CLKIN) begin // every time the clk wire has a positive edge, do:
		if(TRGIN) begin // signals new block
			counter <= 0;
			end
		dout[counter] <= DIN; // shift in new data
		counter <= counter + 1;
		end
	
	assign DOUT = dout; // serial output is the out value

endmodule